import { expect as expectCDK, haveResource, SynthUtils, haveOutput } from "@aws-cdk/assert";
import StaticSite, { StaticSiteProps } from "./StaticSite";
import { Stack } from "@aws-cdk/core";

describe("StaticSite", () => {
    const deafultDomainName = "example.com";
    const defaultSiteSubDomain = "subdomain";
    const siteAssetsPath = "./exampleSite";

    const env = {
        account: "CDK_DEFAULT_ACCOUNT",
        region: "eu-west-1",
    };

    let staticSiteProps: StaticSiteProps;

    describe("domainName", () => {
        const testStackName = "domainName-test";

        beforeEach(() => {
            staticSiteProps = {
                domainName: deafultDomainName,
                siteAssetsPath: siteAssetsPath,
            };
        });

        it("should match snapshot", () => {
            const stack = new Stack(undefined, undefined, {
                env: env,
            });

            new StaticSite(stack, testStackName, staticSiteProps);

            expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
        });
    });

    describe("siteSubDomain", () => {
        const testStackName = "subDomainName-test";

        describe("when undefined", () => {
            beforeEach(() => {
                staticSiteProps = {
                    domainName: deafultDomainName,
                    siteAssetsPath: siteAssetsPath,
                };
            });

            it("should match snapshot", () => {
                const stack = new Stack(undefined, undefined, {
                    env: env,
                });

                new StaticSite(stack, testStackName, staticSiteProps);

                expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
            });

            it("should set bucket name to be the domain name", () => {
                const stack = new Stack(undefined, undefined, {
                    env: env,
                });

                new StaticSite(stack, testStackName, staticSiteProps);

                expectCDK(stack).to(
                    haveResource("AWS::S3::Bucket", {
                        BucketName: deafultDomainName,
                        WebsiteConfiguration: {
                            ErrorDocument: "index.html",
                            IndexDocument: "index.html",
                        },
                    })
                );
            });
        });

        describe("when defined", () => {
            beforeEach(() => {
                staticSiteProps = {
                    siteSubDomain: defaultSiteSubDomain,
                    domainName: deafultDomainName,
                    siteAssetsPath: siteAssetsPath,
                };
            });

            it("should match snapshot", () => {
                const stack = new Stack(undefined, undefined, {
                    env: env,
                });

                new StaticSite(stack, testStackName, staticSiteProps);

                expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
            });

            it("should set bucket name to be 'subdomain.domain'", () => {
                const stack = new Stack(undefined, undefined, {
                    env: env,
                });

                new StaticSite(stack, testStackName, staticSiteProps);

                expectCDK(stack).to(
                    haveResource("AWS::S3::Bucket", {
                        BucketName: `${defaultSiteSubDomain}.${deafultDomainName}`,
                        WebsiteConfiguration: {
                            ErrorDocument: "index.html",
                            IndexDocument: "index.html",
                        },
                    })
                );
            });
        });
    });

    describe("siteAssetsPath", () => {
        const testStackName = "siteAssetsPath-test";

        beforeEach(() => {
            staticSiteProps = {
                domainName: deafultDomainName,
                siteAssetsPath: siteAssetsPath,
            };
        });

        it("should not throw error if path exists", () => {
            const stack = new Stack(undefined, undefined, {
                env: env,
            });

            new StaticSite(stack, testStackName, staticSiteProps);
        });

        it("should throw error if path does not exist", () => {
            const stack = new Stack(undefined, undefined, {
                env: env,
            });

            staticSiteProps.siteAssetsPath = "./pathshouldnotexist_iondfvoinwekj";

            expect(() => {
                new StaticSite(stack, testStackName, staticSiteProps);
            }).toThrow();
        });
    });

    describe("constructConfig", () => {
        describe("useRoute53", () => {
            const testStackName = "useRoute53-test";

            describe("when true", () => {
                beforeEach(() => {
                    staticSiteProps = {
                        domainName: deafultDomainName,
                        siteAssetsPath: siteAssetsPath,
                        constructConfig: {
                            useRoute53: true,
                        },
                    };
                });

                it("should create a zone output with value equal to the domain", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expectCDK(stack).to(
                        haveOutput({
                            outputName: "useRoute53testSiteE653AD68",
                            outputValue: `https://${deafultDomainName}`,
                        })
                    );
                });

                it("should match snapshot", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                });

                describe("when useCloudfront is true", () => {
                    beforeEach(() => {
                        staticSiteProps = {
                            domainName: deafultDomainName,
                            siteAssetsPath: siteAssetsPath,
                            constructConfig: {
                                useRoute53: true,
                                useCloudfront: true,
                            },
                        };
                    });

                    it("should match snapshot", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                    });

                    it("should create an ARecord targeting the cloudfront distribution", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expectCDK(stack).to(
                            haveResource("AWS::Route53::RecordSet", {
                                Type: "A",
                                Name: `${deafultDomainName}.`,
                                HostedZoneId: "DUMMY",
                                AliasTarget: {
                                    DNSName: {
                                        "Fn::GetAtt": [
                                            "useRoute53testSiteDistributionCFDistribution8800E84E",
                                            "DomainName",
                                        ],
                                    },
                                    HostedZoneId: "Z2FDTNDATAQYW2",
                                },
                            })
                        );
                    });
                });

                describe("when cloudfront is false", () => {
                    beforeEach(() => {
                        staticSiteProps = {
                            domainName: deafultDomainName,
                            siteAssetsPath: siteAssetsPath,
                            constructConfig: {
                                useRoute53: true,
                                useCloudfront: false,
                            },
                        };
                    });

                    it("should match snapshot", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                    });

                    it("should create an ARecord targeting the s3 bucket", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expectCDK(stack).to(
                            haveResource("AWS::Route53::RecordSet", {
                                Type: "A",
                                Name: `${deafultDomainName}.`,
                                HostedZoneId: "DUMMY",
                                AliasTarget: {
                                    DNSName: "s3-website-eu-west-1.amazonaws.com",
                                    HostedZoneId: "Z1BKCTXD74EZPE",
                                },
                            })
                        );
                    });
                });
            });
        });

        // TODO:
        describe("useCloudfront", () => {
            const testStackName = "useCloudfront-test";

            describe("when true", () => {
                beforeEach(() => {
                    staticSiteProps = {
                        domainName: deafultDomainName,
                        siteAssetsPath: siteAssetsPath,
                        constructConfig: {
                            useCloudfront: true,
                        },
                    };
                });

                it("should match snapshot", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                });

                it("should set bucket to use distribution", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expectCDK(stack).to(
                        haveResource("Custom::CDKBucketDeployment", {
                            DistributionId: {
                                Ref: "useCloudfronttestSiteDistributionCFDistribution1A89703E",
                            },
                            DistributionPaths: ["/*"],
                        })
                    );
                });

                describe("when useRoute53 is true", () => {
                    beforeEach(() => {
                        staticSiteProps = {
                            domainName: deafultDomainName,
                            siteAssetsPath: siteAssetsPath,
                            constructConfig: {
                                useCloudfront: true,
                                useRoute53: true,
                            },
                        };
                    });

                    it("should match snapshot", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                    });

                    it("should output a TLS certificate", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expectCDK(stack).to(
                            haveOutput({
                                outputName: "useCloudfronttestCertificate0C1EA333",
                                outputValue: {
                                    "Fn::GetAtt": [
                                        "useCloudfronttestSiteCertificateCertificateRequestorResourceFA6D1451",
                                        "Arn",
                                    ],
                                },
                            })
                        );
                    });

                    it("should create a cloudfront distribution with certificate", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expectCDK(stack).to(
                            haveResource("AWS::CloudFront::Distribution", {
                                DistributionConfig: {
                                    Aliases: [deafultDomainName],
                                    CustomErrorResponses: [
                                        {
                                            ErrorCode: 403,
                                            ResponseCode: 200,
                                            ResponsePagePath: "/index.html",
                                        },
                                        {
                                            ErrorCode: 404,
                                            ResponseCode: 200,
                                            ResponsePagePath: "/index.html",
                                        },
                                    ],

                                    DefaultCacheBehavior: {
                                        AllowedMethods: ["GET", "HEAD"],
                                        CachedMethods: ["GET", "HEAD"],
                                        Compress: true,
                                        ForwardedValues: {
                                            Cookies: {
                                                Forward: "none",
                                            },
                                            QueryString: false,
                                        },
                                        TargetOriginId: "origin1",
                                        ViewerProtocolPolicy: "redirect-to-https",
                                    },
                                    DefaultRootObject: "index.html",
                                    Enabled: true,
                                    HttpVersion: "http2",
                                    IPV6Enabled: true,
                                    Origins: [
                                        {
                                            DomainName: {
                                                "Fn::GetAtt": [
                                                    "useCloudfronttestexamplecom655AF68B",
                                                    "RegionalDomainName",
                                                ],
                                            },
                                            Id: "origin1",
                                            S3OriginConfig: {},
                                        },
                                    ],
                                    PriceClass: "PriceClass_100",
                                    ViewerCertificate: {
                                        AcmCertificateArn: {
                                            "Fn::GetAtt": [
                                                "useCloudfronttestSiteCertificateCertificateRequestorResourceFA6D1451",
                                                "Arn",
                                            ],
                                        },
                                        MinimumProtocolVersion: "TLSv1.1_2016",
                                        SslSupportMethod: "sni-only",
                                    },
                                },
                            })
                        );
                    });
                });

                describe("when useRoute53 is false", () => {
                    beforeEach(() => {
                        staticSiteProps = {
                            domainName: deafultDomainName,
                            siteAssetsPath: siteAssetsPath,
                            constructConfig: {
                                useCloudfront: true,
                                useRoute53: false,
                            },
                        };
                    });

                    it("should match snapshot", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                    });

                    it("should create a cloudfront distribution", () => {
                        const stack = new Stack(undefined, undefined, {
                            env: env,
                        });

                        new StaticSite(stack, testStackName, staticSiteProps);

                        expectCDK(stack).to(
                            haveResource("AWS::CloudFront::Distribution", {
                                DistributionConfig: {
                                    CustomErrorResponses: [
                                        {
                                            ErrorCode: 403,
                                            ResponseCode: 200,
                                            ResponsePagePath: "/index.html",
                                        },
                                        {
                                            ErrorCode: 404,
                                            ResponseCode: 200,
                                            ResponsePagePath: "/index.html",
                                        },
                                    ],
                                    DefaultCacheBehavior: {
                                        AllowedMethods: ["GET", "HEAD"],
                                        CachedMethods: ["GET", "HEAD"],
                                        Compress: true,
                                        ForwardedValues: {
                                            Cookies: {
                                                Forward: "none",
                                            },
                                            QueryString: false,
                                        },
                                        TargetOriginId: "origin1",
                                        ViewerProtocolPolicy: "redirect-to-https",
                                    },
                                    DefaultRootObject: "index.html",
                                    Enabled: true,
                                    HttpVersion: "http2",
                                    IPV6Enabled: true,
                                    Origins: [
                                        {
                                            DomainName: {
                                                "Fn::GetAtt": [
                                                    "useCloudfronttestexamplecom655AF68B",
                                                    "RegionalDomainName",
                                                ],
                                            },
                                            Id: "origin1",
                                            S3OriginConfig: {},
                                        },
                                    ],
                                    PriceClass: "PriceClass_100",
                                    ViewerCertificate: {
                                        CloudFrontDefaultCertificate: true,
                                    },
                                },
                            })
                        );
                    });
                });
            });
        });

        describe("useDeletableBucket", () => {
            const testStackName = "useDeletableBucket-test";

            describe("when true", () => {
                beforeEach(() => {
                    staticSiteProps = {
                        domainName: deafultDomainName,
                        siteAssetsPath: siteAssetsPath,
                        constructConfig: {
                            useDeletableBucket: true,
                        },
                    };
                });

                it("should match snapshot", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                });

                it("should create a AutoDeleteBucket resource", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expectCDK(stack).to(
                        haveResource("Custom::AutoDeleteBucket", {
                            BucketName: {
                                Ref: "useDeletableBuckettestexamplecom3B4B54CD",
                            },
                            ServiceToken: {
                                "Fn::GetAtt": [
                                    "AutoBucket7677dc81117d41c0b75bdb11cb84bb70DC15ED41",
                                    "Arn",
                                ],
                            },
                        })
                    );
                });
            });

            describe("when false", () => {
                beforeEach(() => {
                    staticSiteProps = {
                        domainName: deafultDomainName,
                        siteAssetsPath: siteAssetsPath,
                        constructConfig: {
                            useDeletableBucket: false,
                        },
                    };
                });

                it("should match snapshot", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                });

                it("should create a deafult s3 bucket resource", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expectCDK(stack).to(
                        haveResource("AWS::S3::Bucket", {
                            BucketName: deafultDomainName,
                            WebsiteConfiguration: {
                                ErrorDocument: "index.html",
                                IndexDocument: "index.html",
                            },
                        })
                    );
                });
            });
        });

        describe("when useRoute53 and useCloudfront are false", () => {
            const testStackName = "useRoute53-useCloudfront-test";

            describe("when true", () => {
                beforeEach(() => {
                    staticSiteProps = {
                        domainName: deafultDomainName,
                        siteAssetsPath: siteAssetsPath,
                        constructConfig: {
                            useRoute53: false,
                            useDeletableBucket: false,
                        },
                    };
                });

                it("should match snapshot", () => {
                    const stack = new Stack(undefined, undefined, {
                        env: env,
                    });

                    new StaticSite(stack, testStackName, staticSiteProps);

                    expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
                });
            });
        });
    });
});
