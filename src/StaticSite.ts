import { DnsValidatedCertificate } from "@aws-cdk/aws-certificatemanager";
import {
    CloudFrontWebDistribution,
    SSLMethod,
    SecurityPolicyProtocol,
    SourceConfiguration,
    CfnDistribution,
} from "@aws-cdk/aws-cloudfront";
import { HostedZone, ARecord, AddressRecordTarget, IHostedZone } from "@aws-cdk/aws-route53";
import { CloudFrontTarget, BucketWebsiteTarget } from "@aws-cdk/aws-route53-targets/lib";
import { Bucket, BucketProps } from "@aws-cdk/aws-s3";
import { AutoDeleteBucket } from "@mobileposse/auto-delete-bucket";
import { BucketDeployment, Source } from "@aws-cdk/aws-s3-deployment";
import { Construct, CfnOutput, RemovalPolicy } from "@aws-cdk/core";
import assignDefaultValuesToObject from "assign-default-values-to-object";

export interface IConstructConfig {
    /**
     * Whether the construct should create a route53 record or not.
     *
     * @default true
     **/
    useRoute53?: boolean;
    /**
     * Whether the construct should create a cloudfront distribution or not.
     *
     * @default true
     **/
    useCloudfront?: boolean;
    /**
     * Whether the bucket should be deleteable on cdk destroy or not.
     *
     * @default false
     **/
    useDeletableBucket?: boolean;
}

const deafultConstructConfig: IConstructConfig = {
    useRoute53: true,
    useCloudfront: true,
    useDeletableBucket: false,
};

export interface StaticSiteProps {
    /**
     * Sub domain to use - added as a prefix to the domain name.
     **/
    siteSubDomain?: string;
    /**
     * Domain name to use. Should be the value of a hosted zone.
     **/
    domainName: string;
    /**
     * Location of the directory containing the files to be hosted.
     **/
    siteAssetsPath: string;
    /**
     * Configuration for the constructs to use.
     **/
    constructConfig?: IConstructConfig;
}

export default class StaticSite extends Construct {
    constructor(scope: Construct, id: string, props: StaticSiteProps) {
        super(scope, id);

        const { domainName, siteSubDomain, siteAssetsPath, constructConfig = {} } = props;

        const _constructConfig = assignDefaultValuesToObject(
            deafultConstructConfig,
            constructConfig
        );
        const { useRoute53, useCloudfront, useDeletableBucket } = _constructConfig;

        const indexPage = "index.html";
        const errorPage = "index.html";

        const siteDomain =
            typeof siteSubDomain === "undefined" ? domainName : siteSubDomain + "." + domainName;

        let zone: IHostedZone | undefined = undefined;
        if (useRoute53) {
            zone = HostedZone.fromLookup(this, "Zone", { domainName: domainName });
            new CfnOutput(this, "Site", { value: "https://" + siteDomain });
        }

        // Content bucket
        const bucketName = siteDomain;
        const bucketProps: BucketProps = {
            bucketName: bucketName,
            publicReadAccess: true,
            websiteIndexDocument: indexPage,
            websiteErrorDocument: errorPage,
            removalPolicy: RemovalPolicy.DESTROY,
        };
        const siteBucket = useDeletableBucket
            ? new AutoDeleteBucket(this, bucketName, bucketProps)
            : new Bucket(this, bucketName, bucketProps);
        new CfnOutput(this, "Bucket", { value: siteBucket.bucketName });

        // Cloudfront distribution
        let distribution: CloudFrontWebDistribution | undefined = undefined;
        if (useCloudfront) {
            const originConfigs: SourceConfiguration[] = [
                {
                    s3OriginSource: {
                        s3BucketSource: siteBucket,
                    },
                    behaviors: [{ isDefaultBehavior: true }],
                },
            ];

            const errorConfigurations: CfnDistribution.CustomErrorResponseProperty[] = [
                { errorCode: 403, responsePagePath: `/${errorPage}`, responseCode: 200 },
                { errorCode: 404, responsePagePath: `/${errorPage}`, responseCode: 200 },
            ];

            if (typeof zone !== "undefined") {
                // TLS certificate
                const certificateArn = new DnsValidatedCertificate(this, "SiteCertificate", {
                    domainName: siteDomain,
                    hostedZone: zone,
                    region: "us-east-1",
                }).certificateArn;
                new CfnOutput(this, "Certificate", { value: certificateArn });

                // CloudFront distribution that provides HTTPS
                distribution = new CloudFrontWebDistribution(this, "SiteDistribution", {
                    aliasConfiguration: {
                        acmCertRef: certificateArn,
                        names: [siteDomain],
                        sslMethod: SSLMethod.SNI,
                        securityPolicy: SecurityPolicyProtocol.TLS_V1_1_2016,
                    },
                    originConfigs: originConfigs,
                    errorConfigurations: errorConfigurations,
                });
                new CfnOutput(this, "DistributionId", { value: distribution.distributionId });
            } else {
                // CloudFront distribution that provides HTTPS
                distribution = new CloudFrontWebDistribution(this, "SiteDistribution", {
                    originConfigs: originConfigs,
                    errorConfigurations: errorConfigurations,
                });
                new CfnOutput(this, "DistributionId", { value: distribution.distributionId });
            }
        }

        // Route53 alias record for the CloudFront distribution
        if (typeof zone !== "undefined") {
            new ARecord(this, "SiteAliasRecord", {
                recordName: siteDomain,
                target:
                    typeof distribution !== "undefined"
                        ? AddressRecordTarget.fromAlias(new CloudFrontTarget(distribution))
                        : AddressRecordTarget.fromAlias(new BucketWebsiteTarget(siteBucket)),
                zone,
            });
        }

        // Deploy site contents to S3 bucket
        new BucketDeployment(this, "DeployWithInvalidation", {
            sources: [Source.asset(siteAssetsPath)],
            destinationBucket: siteBucket,
            distribution,
            distributionPaths: useCloudfront ? ["/*"] : undefined,
        });
    }
}
