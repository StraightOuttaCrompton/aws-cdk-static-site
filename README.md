# [Aws-cdk](https://github.com/aws/aws-cdk) static site construct

<!-- Add badge when skipped builds are not shown in badge - https://gitlab.com/gitlab-org/gitlab/issues/15304 -->
<!-- [![pipeline status](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commits/master) -->

[![coverage report](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commits/master)

An [npm module](https://www.npmjs.com/package/aws-cdk-static-site) exporting a configurable static site construct.

## Installing

```sh
npm i aws-cdk-static-site
```

## Basic Usage

```ts
import * as cdk from "@aws-cdk/core";
import StaticSite from "aws-cdk-static-site";

export default class StaticSiteStack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props: cdk.StackProps) {
        super(scope, id, props);

        new StaticSite(this, id, {
            domainName: "example.com",
            siteAssetsPath: "./exampleSiteDirectory"
        });
    }
}
```

## Specifying a subdomain

You can specify a subdomain using the `siteSubDomain` property.

```ts
new StaticSite(this, id, {
    siteSubDomain: "www",
    domainName: "example.com",
    siteAssetsPath: "./exampleSiteDirectory"
});
```

## Specifying constructs

You can specify which constructs will be used with the `constructConfig` property.

```ts
new StaticSite(this, id, {
    domainName: "example.com",
    siteAssetsPath: "./exampleSiteDirectory",
    constructConfig: {
        useRoute53: true,
        useCloudfront: true,
        useDeletableBucket: false
    }
});
```

The construct config specified above is the default.

## Links

-   Static site construct inspired by the [static-site example](https://github.com/aws-samples/aws-cdk-examples/tree/master/typescript/static-site).
-   CI/CD setup using [this post](https://vgarcia.dev/blog/2019-11-06--publish-a-package-to-npm-step-by-step/).
