# 1.0.0 (2022-09-19)


### Bug Fixes

* added cloudfront error page ([ab7f63d](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/ab7f63dca541d78c14a804287bb0773d8d7823ed))
* added dependencies job to release dependencies ([aab604e](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/aab604edd105f1f407adb9e6249b4b3487d81553))
* added release-notes-generator ([95b7046](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/95b70464f4a877e581dfb45f92b25e08e28a86ec))
* added responseCode to cloudfront custom errors ([66e26a4](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/66e26a411d72b0c4530051b0b4f4639ab7582a3a))
* changes construct scope from app to construct ([de67866](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/de678661823a7d719ca39cdd0ed6ea08caa48dd1))
* dependency needed to be job name not stage ([b9a5269](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/b9a526932d1740f272ff22f22edd5906245c7e28))
* dist is now root of published package ([afba0f4](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/afba0f404bbd231f3427d4d3ac5f48e347b3ba3b))
* environment variable NPM_TOKEN is no longer required ([f03826c](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/f03826cddff78c0a4339b0bcc17426d58eb078fe))
* exporting props ([ba5d86e](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/ba5d86e7e0ad716c771661e6e1bae6dd3b2db47a))
* package.json & package-lock.json to reduce vulnerabilities ([7305c3b](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/7305c3b53ef4de028f5c87e32b953932dae53ff3))
* pointing to /index.html for cloudfront error page ([fbc5c53](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/fbc5c53b8ad12a9fea031529041af526f72426f7))
* reverted silly decision to rename buckets ([1cbb72f](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/1cbb72f28f32a30cf9a3451905f32a67b748c678))
* set DnsValidatedCertificate to be us-east-1 ([eae5400](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/eae5400269a130594b946215719f644ae25ea5a6))


### Features

* added cloud front and route 53 ([23d0c7a](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/23d0c7a8b1c657ae39f1bb7fe3cda07ff3bb3098))
* added console log to StaticSite for package release test ([1dc0efd](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/1dc0efd403033dac595e4029810bf12afb1dcf6b))
* added deletableBucket option ([8cfa495](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/8cfa495ae68acc0c257f0cbfd7453a70e3cec1d5))
* added domainName and siteSubDomain props ([3141971](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/314197132b3fdba85645e7e1636627a77a50397d))
* added IConstructConfig ([b548b52](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/b548b521f3a48208180b6a9410e2ada154d82e45))
* added route53 static site without cloudfront ([0508150](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/05081505f56e81bec7970dd9a2ba15c1b59ea12e))
* constructConfig now has default values ([21a2263](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/21a22634d7ea08a327aae296579cee4235862fb8))
* removed console log from StaticSite for package release test ([0e6f6c9](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/0e6f6c99bb49b59e5fcac5e7bbd056f0462aca4f))
* removed domain name from bucket name if subdomain is defined ([910446f](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/910446f37c13f7a3c785ab6b1d8dd84251f77f26))
* siteSubDomain is optional ([6bfe3e7](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/6bfe3e77088724044a4abce0f593d729711ee916))

## [1.9.1](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.9.0...v1.9.1) (2020-04-25)


### Bug Fixes

* reverted silly decision to rename buckets ([9245d84](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/9245d840490ab29c22b2d9a78c8e3d2fbc30443f))

# [1.9.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.5...v1.9.0) (2020-04-25)


### Features

* removed domain name from bucket name if subdomain is defined ([fe8d999](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/fe8d9998ea140e0787407efc1e28c180d94e8481))

## [1.8.5](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.4...v1.8.5) (2020-04-12)


### Bug Fixes

* package.json & package-lock.json to reduce vulnerabilities ([bfb2771](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/bfb27714e43da9dd7f1bc50b11d2622df04bac1d))

## [1.8.4](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.3...v1.8.4) (2020-02-18)


### Bug Fixes

* pointing to /index.html for cloudfront error page ([ff8150f](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/ff8150f68a60b76ee1af24d06d8f3ffa85abafeb))

## [1.8.3](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.2...v1.8.3) (2020-02-18)


### Bug Fixes

* added responseCode to cloudfront custom errors ([2ef185f](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/2ef185f820b4459fe1f8dd53b38de68297a40d31))

## [1.8.2](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.1...v1.8.2) (2020-02-18)


### Bug Fixes

* added cloudfront error page ([f621dd2](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/f621dd26328382f2b14c326b98587b353bf499f2))

## [1.8.1](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.8.0...v1.8.1) (2020-02-17)

# [1.8.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.7.0...v1.8.0) (2020-02-17)


### Features

* constructConfig now has default values ([bfaf110](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/bfaf1106d54ed436b6c96613f5a8c4a93ad49b80))

# [1.7.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.6.0...v1.7.0) (2020-02-16)


### Features

* siteSubDomain is optional ([946a750](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/946a7503dd74da8d5e58570931077ed4976d9d4b))

# [1.6.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.5.1...v1.6.0) (2020-02-16)


### Features

* added deletableBucket option ([9e6e5d5](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/9e6e5d5959761875bcc272e2696b8b8ed7072925))

## [1.5.1](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.5.0...v1.5.1) (2020-02-16)

# [1.5.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.4.0...v1.5.0) (2020-02-15)


### Features

* added IConstructConfig ([f70f74f](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/f70f74f8c2c7200aa6f089e3beb0c895448cd140))

# [1.4.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.3.4...v1.4.0) (2020-02-15)


### Features

* added route53 static site without cloudfront ([ee20671](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/ee206712de567b7c3a317132d4d5c18f1efe9612))

## [1.3.4](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.3.3...v1.3.4) (2020-02-11)


### Bug Fixes

* set DnsValidatedCertificate to be us-east-1 ([3bf30e6](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/3bf30e66e8eac932b86d8d3538f3fdd6d9c2acd8))

## [1.3.3](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.3.2...v1.3.3) (2020-02-08)

## [1.3.2](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.3.1...v1.3.2) (2020-02-08)


### Bug Fixes

* environment variable NPM_TOKEN is no longer required ([1bff8ec](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/1bff8ec74ff9ef87b2daee0a54285a1168c523fe))

## [1.3.1](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.3.0...v1.3.1) (2020-02-08)

# [1.3.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.2.0...v1.3.0) (2020-02-07)


### Features

* added domainName and siteSubDomain props ([2198989](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/2198989ba1945a4cd393fb403036cc0e554c0e17))

# [1.2.0](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.1.5...v1.2.0) (2020-02-07)


### Features

* added cloud front and route 53 ([48616df](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/48616df96d5d0274388a33a61e23767666f72bae))

## [1.1.5](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/compare/v1.1.4...v1.1.5) (2020-02-06)


### Bug Fixes

* added release-notes-generator ([e3dd723](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/e3dd723bcdff6e9fe71496c13bbadf323a43b5ff))

# 1.0.0 (2020-02-06)


### Features

* added console log to StaticSite for package release test ([66dfbcb](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site/commit/66dfbcb2ea491f0472c7420a145a9e5bdbdd9c95))
